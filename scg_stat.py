from SCG.joueur import joueur
from SCG.partie import partie


# TODO
#  - Récupérer les données de chaque partie pour pouvoir faire des stat
#  - Pouvoir choisir la fonction de stratégie des joueurs

# jouer N parties
N = 10

for i in range(1, N+1):
    print('partie #', i)
    partie(joueur('robot'), joueur('humain'))