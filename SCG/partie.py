from random import shuffle

# TODO
#  - Garder dans une variable les données de la partie (cartes distribuées, jouées, gagnant etc.)
#  - faire en sorte que partie() retourne cette variable


def partie(joueurRobot, joueurHumain):
    # les cartes
    cartes = [0, 1, 1, 2, 2, 3, 3, 4, 4, 5]

    # mélanger les cartes
    shuffle(cartes)

    # distribuer 5 cartes à chaque joueur
    joueurRobot["main"] = cartes[:5]
    joueurHumain["main"] = cartes[5:]

    # boucle
    # tant qu'il reste des cartes
    while len(joueurRobot["main"]) > 0:
        # début du tour

        # TODO: Choisir la carte en fonction de la stratégie du joueur
        # ici les deux joueurs sont stupides, il joue une carte au hasard (la dernière de la main)
        # Robot joue une carte
        carte_robot = joueurRobot["main"].pop()
        # Humain joue une carte
        carte_humain = joueurHumain["main"].pop()

        # Comparer les cartes
        # TODO: comparer les deux cartes et ajouter un point au gagnant

        # fin du tour

    # déterminer le gagnant
    if joueurRobot["score"] > 2:
        print('Robot gagne')
    else:
        print('Humain gagne')


if __name__ == '__main__':
    from joueur import joueur

    partie(joueur('robot'), joueur('humain'))
