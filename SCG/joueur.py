# TODO:
#  - Il va falloir ajouter un paramètre qui sera la fonction de stratégie du choix de la carte
#  - ensuite on pourra appeler mon_joueur["strategie"]() pour choisir la carte
#  a un moment on se rendra compte qu'il est plus simple d'utiliser la POO.
#  on aura donc mon_joueur.strategie(partie_data)

def joueur(nom):
    return {
        "nom": nom,
        "score": 0,
        "main": [],
        "stratégie": 'blip bloup'
    }


if __name__ == '__main__':
    mon_joueur = joueur("Bob")
    print(mon_joueur)
    mon_joueur["strategie"] = lambda: print('doit prendre en paramètre la "partie" et doit retourner une carte')
    print(mon_joueur)
    mon_joueur["strategie"]()